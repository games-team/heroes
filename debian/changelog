heroes (0.21-20) unstable; urgency=medium

  * Drop the build dependency on libmikmod-config, the build uses
    SDL_mixer rather than LibMikMod when both are available.
    Closes: #1086449.
  * Standards-Version 4.7.0, no change required.

 -- Stephen Kitt <skitt@debian.org>  Thu, 31 Oct 2024 08:37:05 +0100

heroes (0.21-19) unstable; urgency=medium

  * Avoid forward declarations in the configure run test program.
    Closes: #1066661.
  * Bump to debhelper compatibility level 13.
  * Standards-Version 4.6.2, no further change required.
  * Remove references to the long-obsolete heroes-common and heroes-sdl
    packages.

 -- Stephen Kitt <skitt@debian.org>  Thu, 14 Mar 2024 13:07:40 +0100

heroes (0.21-18) unstable; urgency=medium

  * Ensure variables are only declared once. Closes: #957333.
  * Switch to debhelper compatibility level 12.
  * Avoid shipping an empty /usr/share/games/heroes directory.
  * Standards-Version 4.5.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Sat, 16 May 2020 16:29:27 +0200

heroes (0.21-17) unstable; urgency=medium

  * Migrate to Salsa.
  * Since heroes is installed owned by root:games, the binary target in
    debian/rules needs to run with fakeroot; indicate this with “Rules-
    Requires-Root”.
  * Update watch file.
  * Switch to debhelper compatibility level 11.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Sun, 06 May 2018 20:54:19 +0200

heroes (0.21-16) unstable; urgency=medium

  * Move Heroes to the Arcade sub-category in the .desktop file (closes:
    #865837). Thanks to Petter Reinholdtsen for the patch!
  * Drop the obsolete transitional packages, heroes-sdl and heroes-
    common.
  * Update debian/copyright.
  * Standards-Version 4.0.0, no further change required.
  * Switch to debhelper compatibility level 10.

 -- Stephen Kitt <skitt@debian.org>  Fri, 14 Jul 2017 22:43:02 +0200

heroes (0.21-15) unstable; urgency=medium

  * Rewrite debian/copyright in machine-readable format.
  * Migrate the /usr/share/doc/heroes-sdl symlink to a directory using
    dpkg-maintscript-helper (closes: #820323).
  * Clean up debian/control using cme.
  * Stop installing /usr/share/games/heroes in the heroes package.
  * Standards-Version 3.9.8, no further change required.
  * Enable all hardening options.

 -- Stephen Kitt <skitt@debian.org>  Wed, 13 Apr 2016 20:03:17 +0200

heroes (0.21-14) unstable; urgency=medium

  [ Stephen Kitt ]
  * Handle heroes' permissions in the dh_fixperms override, for
    architecture-dependent builds only.
  * Preserve the original manpages, help2man can't rebuild the new ones
    on buildds.
  * Allow building with GCC 6.
  * Avoid attempting to rebuild doc/heroes.texi, the build isn't
    compatible with current autogen (closes: #816269).

  [ Alexandre Detiste ]
  * Remove leftover heroes alternative (closes: #813244).

 -- Stephen Kitt <skitt@debian.org>  Sat, 05 Mar 2016 16:44:22 +0100

heroes (0.21-13) unstable; urgency=medium

  * Handle "Arch: all" builds correctly.

 -- Stephen Kitt <skitt@debian.org>  Sun, 24 Jan 2016 23:34:26 +0100

heroes (0.21-12) unstable; urgency=medium

  * Make sure /var/games/heroes is created (closes: #812517).

 -- Stephen Kitt <skitt@debian.org>  Sun, 24 Jan 2016 18:51:16 +0100

heroes (0.21-11) unstable; urgency=medium

  * New maintainer (closes: #738894).
  * Switch to debhelper compatibility level 9, using dh.
  * Switch to source format "3.0 (quilt)", and split up the old .diff.gz
    patch.
  * Enable the upstream tests, except those which require displaying the
    game.
  * Build a single heroes package since we only care about the SDL
    version (with heroes-common and heroes-sdl transitional packages for
    now).
  * Add homepage.
  * Add "set -e" to the maintainer scripts.
  * Drop the Encoding key from heroes.desktop.
  * Drop the Debian menu file.
  * Standards-Version 3.9.6, no further change required.
  * Spell "suppress" correctly (thanks lintian!).
  * Build with mikmod.
  * Load heroesrc directly from /etc, drop /usr/share/games/heroes/etc.
  * Add VCS URIs.

 -- Stephen Kitt <skitt@debian.org>  Sat, 23 Jan 2016 20:28:10 +0100

heroes (0.21-10) unstable; urgency=medium

  * QA upload
  * build with dh_autotools-dev to fix FTBFS on new arches. Thanks to
    Logan Rosen for the patch! (Closes: #744071):
    - debian/control: add build-dependency on autotools-dev
    - debian/rules: call dh_autotools-dev_{update,restore}config in targets
      build-stamp and clean.

 -- Ralf Treinen <treinen@debian.org>  Fri, 19 Sep 2014 20:42:39 +0200

heroes (0.21-9) unstable; urgency=medium

  * QA upload.
  * The package is orphaned, change the maintainer to Debian QA Group.
  * Drop the build-dependency on libmikmod2-dev, the package explicitly
    configures without mikmod.
  * Fix the segfault on free when levels aren't loaded in heroeslvl
    (closes: #716016).
  * Avoid installing /usr/share/info/dir (closes: #597415).
  * Update debian/watch using Bart Martens' version.
  * Add ${misc:Depends} to heroes-sdl and heroes-common.
  * Remove dh_desktop from debian/rules.

 -- Stephen Kitt <skitt@debian.org>  Mon, 17 Mar 2014 20:58:57 +0100

heroes (0.21-8.4) unstable; urgency=low

  * Non-maintainer upload.
  * Drop heroes-ggi binary package and the underlying build-dep
    on libggi-dev, which is going away (Closes: #680239, #84756)

 -- Moritz Muehlenhoff <jmm@debian.org>  Thu, 05 Jul 2012 21:23:49 +0200

heroes (0.21-8.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS on big-endian arches.
    + Use __swabX from linux/swab.h instead of __arch_swabX.
    + Thanks to Raf Czlonka for testing the resulting binaries.
    + Closes: #660236
  * Drop depends of heroes-common on heroes-sdl|heroes-ggi to recommends.
    + Closes: #626320
  * Tighten depens of heroes-sdl and heroes-ggi on heroes-common to
    = ${binary:Version}.

 -- Evgeni Golov <evgeni@debian.org>  Sat, 03 Mar 2012 16:29:31 +0100

heroes (0.21-8.2) unstable; urgency=low

  * Non-maintainer upload.
  * FTBFS with binutils-gold (Closes: #554799)
    - aclocal.m4: tmp_LIBS uses $LIBS instead of $CPPLIBS.
      thanks to Paul Merrill <napalminc@gmail.com>.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sun, 08 Jan 2012 22:37:30 +0900

heroes (0.21-8.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Adding texinfo to build depends (Closes: #467077)
  * Urgency medium for rc bug fix (only medium, because it's just a game)
  * Added version to debian/watch
  * Added dh_desktop call to debian/control
  * bumped standards to 3.8.0 (no further changes needed)
  * last three changes to fix some of the lintian warnings; shouldn't have any
    side effects

 -- Alexander Reichle-Schmehl <tolimar@debian.org>  Sun, 07 Sep 2008 12:12:01 +0200

heroes (0.21-8) unstable; urgency=low

  * Promote the dependency of heroes-ggi and heroes-sdl on
    heroes-sound-tracks to Recommends. (Closes: #455595)
  * Add heroes-common to the list of things touched by
    dh_fixperms, for paranoia's sake.
  * Move Heroes into Games/Action instead of the deprecated Games/Arcade.
  * Remove doc/heroes.info in clean (CLoses: #357235).
  * Add ${shlibs:Depends} to the Depends line of heroes-common, since it
    contains binaries.
  * Use "[ ! -f Makefile ] || $(MAKE) distclean" instead of
    "-$(MAKE) distclean" in the "clean" target to make lintian happy.
  * Use ${binary:Version} to depend on heroes-common instead of
    ${Source-Version}. (eliminates another lintian warning)

 -- Daniel Burrows <dburrows@debian.org>  Sun, 03 Feb 2008 18:53:11 -0800

heroes (0.21-7) unstable; urgency=low

  * Hack Makefile.in (NOT Makefile.am as we no longer have an automake
    capable of parsing it in Debian!) to not link heroeslvl against
    $(LIBS).  It doesn't need a dependency on SDL and so on, since
    it's just a command-line utility.

    The implementation of this is pretty horrid.  I'll email upstream
    and let them know, but they don't seem to be putting out releases
    anymore.

  * Add sv.po. (Closes: #348253)

  * Add a .desktop entry.  Apparently all the cool kids have one now.
    (Closes: #384742)

  * Acknowledge NMU (Closes: #381272).

 -- Daniel Burrows <dburrows@debian.org>  Mon, 25 Sep 2006 21:14:44 -0700

heroes (0.21-6.1) unstable; urgency=medium

  * NMU as part of the libgii/libggi transition.
  * Changed build-dependency on libggi2-dev to libggi2-dev (>= 1:2.2.1-4).
    Closes: #381272.

 -- Anibal Monsalve Salazar <anibal@debian.org>  Wed, 09 Aug 2006 08:46:18 +1000

heroes (0.21-6) unstable; urgency=low

  * Update config.guess and config.sub (Closes: #342421).
  * Update debhelper compat level to 5.
  * Build-depend on heroes-data. (HACK)

    This is necessary because upstream uses "heroes --help" to generate
    the manpage, but this command fails if the data files for heroes are
    not available; worse, the failure mode is to generate an invalid
    manpage!

 -- Daniel Burrows <dburrows@debian.org>  Wed,  7 Dec 2005 10:25:52 -0800

heroes (0.21-5) unstable; urgency=low

  * Acknowledge 4.1 NMU (Closes: #235714).
  * Add autogen and help2man to Build-Dependencies.
  * Force regeneration of the manpages.
  * Patch for g++-4.0 FTBFS (Closes: #297314).
  * Apply patch from Jens Seidel to de.po (Closes: #314046).
  * Explicitly call the GGI and/or SDL version of heroes in the
    corresponding menu file.
  * Policy/lintian updates:
    - Honor DEB_BUILD_OPTIONS=noopt
    - Quote strings in menu files
    - Don't mention sh in build-dep specifications
    - In fact, drop all arch exclusions for the time being to
      see whether they're really needed.
    - Fix deprecated chown invocations (root.games -> root:games).
    - Move menu icons to /usr/share/pixmaps.
  * Bump Standards-Version to 3.6.2.0.

 -- Daniel Burrows <dburrows@debian.org>  Tue,  9 Aug 2005 09:40:27 -0700

heroes (0.21-4.1) unstable; urgency=low

  * NMU (with maintainer consent)
  * Patch doc/Makefile.in to stop shipping /usr/share/info/dir(.old).gz,
    caused by old automake/new dpkg interaction. (Closes: #235714)

 -- Andreas Metzler <ametzler@debian.org>  Sun,  2 May 2004 00:38:32 +0200

heroes (0.21-4) unstable; urgency=low

  * Move heroesrc to /etc: it is a configuration file, albiet not a
    particularly interesting one.  (Closes: #231362)

 -- Daniel Burrows <dburrows@debian.org>  Sat, 21 Feb 2004 20:29:58 -0500

heroes (0.21-3) unstable; urgency=low

  * Apply upstream patch to fix several bugs:
    - Fixes segfaults in the first level.  (Closes: #169525)
    - Fixes (unreported?) segfault on PPC.
    - Fix to joystick behavior (the same as below)
    - Fixes typo in speed adjustment.

    To apply this patch, I had to revert the joystick patch mentioned
    below, but the patch includes a fix for that problem.

 -- Daniel Burrows <dburrows@debian.org>  Wed, 20 Nov 2002 12:21:09 -0500

heroes (0.21-2) unstable; urgency=low

  * Apply upstream patch to fix slightly odd joystick-related behavior.

 -- Daniel Burrows <dburrows@debian.org>  Sun, 16 Jun 2002 14:55:13 -0400

heroes (0.21-1) unstable; urgency=low

  * New upstream version.
  * Actually packaged as a non-native package (looks like I accidentally
  removed the orig.tar.gz and -4 was built as a native package)

 -- Daniel Burrows <dburrows@debian.org>  Sun, 31 Mar 2002 20:42:30 -0500

heroes (0.20-4) unstable; urgency=low

  * Minor tweak to the Description; hopefully this makes dinstall with
  the below :)

 -- Daniel Burrows <dburrows@debian.org>  Sat,  2 Mar 2002 14:33:35 -0500

heroes (0.20-3) unstable; urgency=low

  * Don't ship /var/games/heroes/scores in the package!  This avoids the
  syndrome where every upgrade resets the high score list.

 -- Daniel Burrows <dburrows@debian.org>  Sat,  2 Mar 2002 10:19:21 -0500

heroes (0.20-2) unstable; urgency=low

  * Actually use the global score file (oops)
  * Tightened permissions:
    - The score file should now be owned by root.games, mode 664.
    - The directory /var/games/heroes is now owned by root.root and
    unwritable by anyone else.  (users should not need to create any files
    here, as /var/games/heroes/scores is shipped in the package)

 -- Daniel Burrows <dburrows@debian.org>  Sat,  2 Mar 2002 09:23:34 -0500

heroes (0.20-1) unstable; urgency=low

  * New upstream version.
  * Kill the patch stuff, it was broken and not worth the trouble.

 -- Daniel Burrows <dburrows@debian.org>  Fri,  8 Feb 2002 00:56:07 -0500

heroes (0.19-4) unstable; urgency=low

  * Applied patch to attempt to fix responsiveness (this uses a
  separated-patch system now to try to avoid the "argh, stuff got
  left behind in the diff" problem)
    Closes: #131898

 -- Daniel Burrows <dburrows@debian.org>  Wed,  6 Feb 2002 16:30:03 -0500

heroes (0.19-3) unstable; urgency=low

  * Version the mikmod dependency, just in case.
  * libmikmod-config --ldadd is brain-damaged.
    Just add -lpthread to the LIBS line until it's fixed :-/

 -- Daniel Burrows <dburrows@debian.org>  Sat,  2 Feb 2002 08:57:40 -0500

heroes (0.19-2) unstable; urgency=low

  * Applied upstream patch to configure to work around change in
  libmikmod-config.

 -- Daniel Burrows <dburrows@debian.org>  Fri,  1 Feb 2002 09:03:51 -0500

heroes (0.19-1) unstable; urgency=low

  * New upstream release
    - Workaround for GGI bugs: use "-G not8"
      (this is a workaround to #84756 and #104493, which are now reassigned
       to libggi2)
  * heroes-sdl and heroes-ggi *both* depend on the corrent -data package now!
  * Build GGI on powerpc now, so this goes into testing.  Also added
   build-conflicts lines for targets which should NOT be built, to avoid
   this problem in the future.

 -- Daniel Burrows <dburrows@debian.org>  Mon, 28 Jan 2002 14:35:56 -0500

heroes (0.18-2) unstable; urgency=low

  * Update config.guess and config.sub (Closes: #127543)

 -- Daniel Burrows <dburrows@debian.org>  Fri, 11 Jan 2002 17:46:49 -0500

heroes (0.18-1) unstable; urgency=medium

  * New upstream release.  Note that this requires heroes-data 1.4.

  * The s390 people enabled sdl on that platform via a binary-only upload;
  apparently SDL has become available since I worked out which platforms
  had what.  Unfortunately, this meant that heroes-sdl was built on s390 in
  0.17-1.0.1, but not in 0.17-2 (since they didn't tell me they had done so)
  The result was that heroes was stuck in testing on all platforms.
    This may have been caused by building heroes in an environment outside
  an autobuilder.  I could insert Build-Conflicts against display targets
  that should not be built on particular platforms, but that's even uglier
  than what I'm doing now.

    Anyway, the SDL version is built on S390 in this release.  Be happy.

 -- Daniel Burrows <dburrows@debian.org>  Thu, 20 Dec 2001 15:28:47 -0500

heroes (0.17-2) unstable; urgency=low

  * Remove /var/games/heroes on purge.  (Closes: #122601)

 -- Daniel Burrows <dburrows@debian.org>  Sun,  9 Dec 2001 11:48:16 -0500

heroes (0.17-1) unstable; urgency=low

  * New upstream release

 -- Daniel Burrows <dburrows@debian.org>  Tue, 20 Nov 2001 13:05:05 -0500

heroes (0.16-2) unstable; urgency=low

  * Applied upstream patch to fix immediate segfault.

 -- Daniel Burrows <dburrows@debian.org>  Sun, 18 Nov 2001 15:57:03 -0500

heroes (0.16-1) unstable; urgency=low

  * New upstream release

 -- Daniel Burrows <dburrows@debian.org>  Sun, 18 Nov 2001 14:43:23 -0500

heroes (0.15-4) unstable; urgency=low

  * Sorry, archive folks, but heroes-common is now arch-any.  I think (hope)
  this will fix the dependency problems that are keeping it out of testing.
  (on the other hand, this eliminates the heroes-utils package....and there
   was much rejoicing)

 -- Daniel Burrows <dburrows@debian.org>  Sat, 17 Nov 2001 10:21:30 -0500

heroes (0.15-3) unstable; urgency=low

  * Applied upstream patch to make worm explosions the right size.
  (the worm's size was being decremented before the explosion was
   generated, so it was one notch too small)

 -- Daniel Burrows <dburrows@debian.org>  Sat, 17 Nov 2001 01:48:52 -0500

heroes (0.15-2) unstable; urgency=low

  * Got a report that this will build on m68k if I don't require
  GGI and mikmod on that platform.  Therefore, they are now disabled.

 -- Daniel Burrows <dburrows@debian.org>  Sat, 17 Nov 2001 00:42:22 -0500

heroes (0.15-1) unstable; urgency=low

  * New upstream release.
    - upstream updated config.guess and config.sub (Closes: #115411)
    - Nasty explosion-related bug (dating to 0.13) fixed.
  * Upstream thinks RMS is evil, is now using version 2 of the GPL
    exclusively.  debian/copyright updated.  (this occured in .14, but
    my package of that was not uploaded and didn't have this update)

 -- Daniel Burrows <dburrows@debian.org>  Sat, 20 Oct 2001 14:37:00 -0400

heroes (0.14-1) unstable; urgency=low

  * New upstream release

 -- Daniel Burrows <dburrows@debian.org>  Fri, 12 Oct 2001 16:03:13 -0400

heroes (0.13-3) unstable; urgency=low

  * Uploaded for the Great SDL shift.

  * Versioned build dependency on libsdl1.2-dev (>= 1.2.2-3.1)
    This is necessary for build daemons to pick the correct SDL to build
  against.  Users building personal packages can ignore this dependency.

  * Hack to work around problems with the new libsdl1.2-mixer package.
  (set LDFLAGS before configuring for SDL)

 -- Daniel Burrows <dburrows@debian.org>  Thu, 11 Oct 2001 16:06:46 -0400

heroes (0.13-2) unstable; urgency=low

  * Applied patch from upstream to use SDL-mixer with SDL again.

 -- Daniel Burrows <dburrows@debian.org>  Thu, 20 Sep 2001 08:45:11 -0400

heroes (0.13-1) unstable; urgency=low

  * New upstream release

 -- Daniel Burrows <dburrows@debian.org>  Wed, 19 Sep 2001 15:08:11 -0400

heroes (0.12-4) unstable; urgency=low

  * Call dh_installinfo so the Info documentation really gets installed.
  This evidently was accidentally dropped in heroes 0.8-3, when the
  arch-independent stuff was split off.
  * Standards-Version bumped to 3.5.6.0 to shut lintian up (no changes
  necessary, nothing in the intervening versions impacts this package)

 -- Daniel Burrows <dburrows@debian.org>  Sat, 25 Aug 2001 11:21:02 -0400

heroes (0.12-3) unstable; urgency=low

  * Blah.  Build-Depends on SDL and GGI are no longer ORed; instead, they
  have arch-exlusions for archs that have one but not the other.  Hopefully,
  this will fix the "heroes isn't going into testing" problem.  (which I
  believe was caused by architectures not always building SDL, since it
  seemed optional)

 -- Daniel Burrows <dburrows@debian.org>  Sun, 12 Aug 2001 12:40:03 -0400

heroes (0.12-2) unstable; urgency=low

  * The locale directories should be correct now.
  * heroes-common now includes /var/games/heroes, with the proper permissions.
  (Closes: #104564, #104538)

 -- Daniel Burrows <dburrows@debian.org>  Wed, 18 Jul 2001 20:49:08 -0400

heroes (0.12-1) unstable; urgency=low

  * New upstream release
  * The package uses DH_COMPAT=3, so it needs to build-depend on
  debhelper (>= 3.0.0)

 -- Daniel Burrows <dburrows@debian.org>  Tue, 10 Jul 2001 16:46:23 -0400

heroes (0.11-1) unstable; urgency=low

  * New upstream release
  * Removed the virtual heroes-bin package.
  heroes-common now has a versioned dependency on one of the available heroes
  versions (installing heroes-common from one Heroes version and binaries
  from another is weird and will break)

 -- Daniel Burrows <dburrows@debian.org>  Sun,  3 Jun 2001 23:58:15 -0400

heroes (0.10-1) unstable; urgency=low

  * New upstream version.
  * This version allows heroes to be sgid games and share high-score files.
  Allow this, and add a debconf question about whether this is desired.
  * Install locale files in /usr/share/locale instead of /usr/share/games/locale
  (so they actually work)

 -- Daniel Burrows <dburrows@debian.org>  Mon,  7 May 2001 08:01:22 -0400

heroes (0.9-5) unstable; urgency=low

  * Switch to libsdl1.2.  (will go into the archive after 0.9-4 gets into
  testing)

 -- Daniel Burrows <dburrows@debian.org>  Mon, 30 Apr 2001 14:42:14 -0400

heroes (0.9-4) unstable; urgency=low

  * Apply Alexandre's patch to fix odd shift-key behavior in some
    circumstances.  (Closes: #94082)

 -- Daniel Burrows <dburrows@debian.org>  Tue, 17 Apr 2001 07:59:38 -0400

heroes (0.9-3) unstable; urgency=low

  * Oops.  Yes, Virginia, the name of the SDL mixer library is
  libsdl-mixer1.1-dev, not libsdl1.1-mixer-dev.  Closes: #91242

 -- Daniel Burrows <dburrows@debian.org>  Sun,  1 Apr 2001 18:32:15 -0400

heroes (0.9-2) unstable; urgency=low

  * Oops.  Somehow the menu files got the wrong names and so weren't being
  included in the package.
  * Eh, we really need debhelper >=2.0.00 to build.  I'm amazed no-one filed
  a bugreport about this..
  * Our Standards-Version should really be 3.1.0, not 3.0.1.
  * Use libsdl1.1-mixer-dev instead of libsdl-mixer-dev in the build-deps
  (it's what should be used, and using the libsdl-mixer-dev could result in
   a build against sdl 1.0, which at least isn't what I intended)

 -- Daniel Burrows <dburrows@debian.org>  Fri, 23 Mar 2001 21:44:57 -0500

heroes (0.9-1) unstable; urgency=low

  * New upstream version.

 -- Daniel Burrows <dburrows@debian.org>  Sat, 24 Feb 2001 11:19:23 -0500

heroes (0.8-3) unstable; urgency=low

  * Tweaked the build so that it's possible to install both the SDL and the
  GGI version at the same time.  This required the addition of a new package,
  heroes-common, which holds files that show up in both versions (eg, the
  manpage)
    /usr/games/heroes-ggi and /usr/games/heroes-sdl are provided as
  alternatives for /usr/games/heroes; SDL is the default if both are installed
  because it seems to work better on average.

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Sun,  4 Feb 2001 08:37:01 -0500

heroes (0.8-2) unstable; urgency=low

  * Oops.  Really set all the architectures to "any" (how'd I miss this for so
   long?)  (closes: #84107)

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Tue, 30 Jan 2001 08:25:34 -0500

heroes (0.8-1) unstable; urgency=low

  * New upstream version.  (sorry about the delay!)

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Fri, 26 Jan 2001 20:14:03 -0500

heroes (0.7-3) unstable; urgency=low

  * Applied upstream patch to avoid the annoying red flash on startup of SDL.

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Sat,  2 Dec 2000 13:05:11 -0500

heroes (0.7-2) unstable; urgency=low

  * Applied upstream patch to fix a segfault when computers tried to do
  certain things (turn 180 degrees, I think from reading it (!) )

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Sat,  2 Dec 2000 12:18:38 -0500

heroes (0.7-1) unstable; urgency=low

  * New upstream version.

  * Added some more magic to the Makefile.  I /believe/ that it should now
  autodetect whether GGI and SDL packages are available in the current build
  environment, and only build the one that's available. (closes: #68699)

  * Full documentation is now installed for both the SDL version and the
  GGI version.

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Wed, 29 Nov 2000 16:48:48 -0500

heroes (0.6-2) unstable; urgency=low

  * Build against SDL 1.1, to see if it works..

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Sun, 29 Oct 2000 21:54:29 -0500

heroes (0.6-1) unstable; urgency=low

  * Final upstream 1.0 release.
  * GGI and SDL targets are now both compiled.  (currently SDL uses sdl_mixer
   instead of libmikmod)  This should be fixed to only compile the package(s)
   which can be compiled on the system (ie -- if GGI isn't present, only SDL
   should build)

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Sat,  2 Sep 2000 15:44:32 -0400

heroes (0.6-0pre3) unstable; urgency=low

  * New upstream version (version number is 0.5pre3 instead of 0.6pre3
   for dpkg's benefit; it thinks 0.6pre3 is bigger than 0.6, go figure)
  * Data files for heroes have been split into a bunch of little packages
  with their own release schedules, which should save a lot of download
  time and allow people to skip parts of the package (eg, the huge music
  tracks).
   (and there was much rejoicing)
  * Architecture field set to i386 for now.

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Mon, 28 Aug 2000 08:31:20 -0400

Heroes (0.5-1) unstable; urgency=low

  * New upstream release (includes level editor)
  * The 'build' target now actually builds (oops)

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Fri, 28 Jul 2000 09:10:14 -0400

heroes (0.4-1) unstable; urgency=low

  * Initial Release.

 -- Daniel Burrows <Daniel_Burrows@brown.edu>  Tue, 18 Jul 2000 09:30:22 -0400
