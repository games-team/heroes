Source: heroes
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Stephen Kitt <skitt@debian.org>
Section: games
Priority: optional
Build-Depends: autogen,
               debhelper-compat (= 13),
               gettext,
               help2man,
               heroes-data,
               libsdl-mixer1.2-dev,
               libsdl1.2-dev,
               texinfo
Standards-Version: 4.7.0
Rules-Requires-Root: binary-targets
Vcs-Browser: https://salsa.debian.org/games-team/heroes
Vcs-Git: https://salsa.debian.org/games-team/heroes.git
Homepage: http://heroes.sourceforge.net

Package: heroes
Architecture: any
Depends: heroes-data,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: heroes-sound-effects,
            heroes-sound-tracks
Description: Collect powerups and avoid your opponents' trails
 Heroes is similar to the "Tron" and "Nibbles" games of yore, but includes
 many graphical improvements and new game features.  In it, you must maneuver
 a small vehicle around a world and collect powerups while avoiding obstacles,
 your opponents' trails, and even your own trail.
 .
 Several styles of play are available, including "get-all-the-bonuses",
 deathmatch, and "squish-the-pedestrians".  All game styles can be played
 in both single-player and two-player (split-screen) modes.
 .
 You should install the heroes-sound-effects package if you want sound effects,
 and the heroes-sound-tracks package if you want background music in the game.
